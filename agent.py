#!/usr/bin/python3
'''
 * Monitorink Agent
 * monitorink.com
 *
 * @author      Mustafa ERCEL  <mustafa.ercel@tsoft.com.tr>
 * @copyright   Tekrom Teknoloji A.S.
 * @version     1.0
 * @since       Python 2.8
'''
import sys
import os
import socket
import signal
import time
import json
import subprocess
from subprocess import PIPE, Popen
from modules.service import Service
import distro

try:
    import psutil
except ImportError:
    print("Please install psutil")
    sys.exit(1)

python_version = sys.version_info[0]
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
Running = True
timeout = 5


class BytesEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            return obj.decode('utf-8')
        return json.JSONEncoder.default(self, obj)


def runCheck():
    global timeout, Running, sock
    updateCheckerStart = 5
    updateCheckerTimer = 60
    service = Service(psutil)
    mainServer = service.getMainServerIp()
    config = mainServer.split(':')
    if len(config) < 1:
        print('IP Cozumlenemedi')
        sys.exit(1)

    serverConfig = (config[0], int(config[1]))
    connect(serverConfig)

    signal.signal(signal.SIGTERM, _handle_signal)
    signal.signal(signal.SIGINT, _handle_signal)

    while Running:
        if (updateCheckerStart * timeout) % updateCheckerTimer == 0:
            print('Checking Update Status')

        updateCheckerStart += 1
        obj = {'name': 'system_info', 'args': [{'data': {}}]}
        obj['args'] = service.load()

        if python_version < 3:
            message = json.dumps(obj, encoding='unicode_escape')
        else:
            message = json.dumps(obj, cls=BytesEncoder)

        try:
            if service.isV3():
                sock.send(message.encode('utf-8'))
            else:
                sock.send(message)
            print('Packet has been sent')
        except Exception as e:
            print(f'Packet send event failed: {e}')
            sock.close()
            time.sleep(10)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            connect(serverConfig)
        time.sleep(timeout)


def _handle_signal(signal, frame):
    global Running
    Running = False


def connect(opts):
    try:
        sock.connect(opts)
    except Exception as e:
        print(f'Could not connect to server: {e}')


def main():
    try:
        runCheck()
    except Exception as e:
        print(f"Error: {e}")
        time.sleep(10)
        runCheck()


if __name__ == '__main__':
    main()